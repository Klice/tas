(assume you have PostgreSQL server with 'postgres' super user)
# createdb -U postgres tas
# psql -U postgres tas < dump.sql
# git clone git@bitbucket.org:Klice/tas.git
# cd tas
# virtualenv .env
# source .env/bin/activate
# pip install -r requirements.txt
# cd tas
# python manage.py migrate
# python manage.py createsuperuser
